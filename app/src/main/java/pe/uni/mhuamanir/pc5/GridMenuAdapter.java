package pe.uni.mhuamanir.pc5;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridMenuAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> text;
    ArrayList<Integer> images;
    int time=10000;
    public GridMenuAdapter(Context context, ArrayList<String> text, ArrayList<Integer> images) {
        this.context = context;
        this.text = text;
        this.images = images;
    }

    @Override
    public int getCount() {
        return text.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_icon_view,parent,false);
        ImageView imageView = view.findViewById(R.id.icon_grid);
        TextView textView = view.findViewById(R.id.tittle_grid);
        imageView.setImageResource(images.get(position));
        textView.setText(text.get(position));
        return view;
    }
}
