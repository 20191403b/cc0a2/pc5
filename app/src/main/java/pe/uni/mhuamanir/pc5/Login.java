package pe.uni.mhuamanir.pc5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class Login extends AppCompatActivity {

    EditText editTextPassword;
    EditText editTextUserName;
    Button buttonLogin,buttonRegister;
    String name="user",password="123a";
    RelativeLayout relativeLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextPassword=findViewById(R.id.password_edit);
        editTextUserName=findViewById(R.id.user_name_edit);
        buttonLogin=findViewById(R.id.button_login);
        buttonRegister=findViewById(R.id.button_register);
        relativeLayout=findViewById(R.id.login);

        buttonLogin.setOnClickListener(v -> {
                if(editTextPassword.getText().toString().equals("")||editTextUserName.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),R.string.login_error,Toast.LENGTH_LONG).show();
                }else if(!editTextPassword.getText().toString().trim().equals(password)||!editTextUserName.getText().toString().trim().equals(name)){
                    Toast.makeText(getApplicationContext(),R.string.incorrect_data,Toast.LENGTH_LONG).show();
                }else{
                    Intent intent=new Intent(Login.this,Menu.class);
                    startActivity(intent);
                    finish();
                }

        });

       buttonRegister.setOnClickListener(v -> Snackbar.make(relativeLayout,R.string.register_error,Snackbar.LENGTH_INDEFINITE).setAction("ok", v1 -> {}).show());

    }



}