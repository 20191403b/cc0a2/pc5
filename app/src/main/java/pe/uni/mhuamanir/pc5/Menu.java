package pe.uni.mhuamanir.pc5;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class Menu extends AppCompatActivity {

    Button buttonExit;
    GridView gridView;
    ArrayList<String> text=new ArrayList<>();
    ArrayList<Integer> image=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        buttonExit=findViewById(R.id.exit);
        gridView=findViewById(R.id.grid_view_menu);
        buttonExit=findViewById(R.id.exit);
        Resources res= getResources();
        gameGrid();
        GridMenuAdapter adapter=new GridMenuAdapter(this,text,image);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            if (position>0) {
                Toast.makeText(getApplicationContext(),R.string.block_game_text,Toast.LENGTH_LONG).show();
            }else{
                Intent intent =new Intent(Menu.this,ZombieGame.class);
                startActivity(intent);
                finish();
            }
        });
        buttonExit.setOnClickListener(v -> {
            AlertDialog.Builder builder= new AlertDialog.Builder(Menu.this);
            builder.setTitle("");
            builder.setCancelable(false);
            builder.setMessage(R.string.alert_exit);

            builder.setPositiveButton(res.getString(R.string.positive), (dialog, which) -> {
                moveTaskToBack(true);
                android.os.Process.killProcess(android.os.Process.myPid());
            });

            builder.setNegativeButton(res.getString(R.string.negative), (dialog, which) -> {


            });

            builder.create().show();

        });

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public void gameGrid(){
        text.add("Zombie game");
        image.add(R.drawable.zombie_logo);
        for(int i=1;i<=5;i++){
            text.add("block");
            image.add(R.drawable.bloqueado);
        }
    }
}