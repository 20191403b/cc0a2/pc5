package pe.uni.mhuamanir.pc5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class Results extends AppCompatActivity {

    TextView textViewPoints,textViewRecord,textViewTittleRecord,textViewGames;
    int points,games,record=0;
    Button buttonMenu,buttonRestart;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        textViewPoints=findViewById(R.id.point_result);
        textViewGames=findViewById(R.id.games_result);
        textViewRecord=findViewById(R.id.record_result);
        textViewTittleRecord=findViewById(R.id.record_text);
        buttonMenu=findViewById(R.id.menu_button);
        buttonRestart=findViewById(R.id.restart_button);

        buttonRestart.setOnClickListener(v -> {
            Intent intent1=new Intent(Results.this,ZombieGame.class);
            startActivity(intent1);
            finish();
        });
        buttonMenu.setOnClickListener(v -> {
            Intent intent2=new Intent(Results.this,Menu.class);
            startActivity(intent2);
            finish();
        });


        points=getIntent().getIntExtra("POINTS",0);
        textViewPoints.setText(String.valueOf(points));
        retrieveData();
        games+=1;
        textViewGames.setText(String.valueOf(games));

        if (points>record) {
            record=points;
            textViewTittleRecord.setText(R.string.new_record);
            textViewTittleRecord.setTextColor(getColor(R.color.gold));
        }
        textViewRecord.setText(String.valueOf(record));
        saveData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveData();
    }

    private void saveData(){
        sharedPreferences =getSharedPreferences("saveGame", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putInt("key record",record);
        editor.putInt("key games",games);
        editor.apply();

    }
    private void retrieveData(){
        sharedPreferences =getSharedPreferences("saveGame",Context.MODE_PRIVATE);
        record=sharedPreferences.getInt("key record",0);
        games=sharedPreferences.getInt("key games",0);
    }

}