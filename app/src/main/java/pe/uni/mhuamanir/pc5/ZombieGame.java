package pe.uni.mhuamanir.pc5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;

public class ZombieGame extends AppCompatActivity {

    TextView textViewTime, textViewPoints,textViewDescription;
    GridView gridView;
    int grid=0,points=0,time=15,maxZombies=20;
    Random r=new Random();
    ArrayList<Integer> images;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zombie_game);
        textViewTime=findViewById(R.id.timer);
        textViewPoints=findViewById(R.id.points);
        gridView=findViewById(R.id.grid_view_game);
        textViewDescription=findViewById(R.id.description_game);
        Resources res= getResources();
        randomZombie();
        textViewTime.setText(String.format(res.getString(R.string.time_count),time));
        textViewPoints.setText(String.format(res.getString(R.string.points),points));
        textViewDescription.setText(String.format(res.getString(R.string.desctiotion_game),time));


        gridView.setOnItemClickListener((parent, view, position, id) -> {
            if (position==grid) {
                randomZombie();
                points+=1;
                textViewPoints.setText(String.format(res.getString(R.string.points),points));
            }
        });

        new CountDownTimer((long)time*1000,1000){

            @Override
            public void onTick(long millisUntilFinished) {
                int timer=1+((int)(millisUntilFinished/1000));
                textViewTime.setText(String.format(res.getString(R.string.time_count),timer));
            }

            @Override
            public void onFinish() {
                Intent intent=new Intent(ZombieGame.this,Results.class);
                intent.putExtra("POINTS",points);
                startActivity(intent);
                finish();
            }
        }.start();
    }


    private  void randomZombie(){
        images=new ArrayList<>();
        grid=r.nextInt(maxZombies);
        for(int i=0;i<maxZombies;i++){
            if(i==grid)images.add(R.drawable.zombie_sprite);
            else images.add(R.drawable.lapida);
        }
        GridGameAdapter adapterGame=new GridGameAdapter(this,images);
        gridView.setAdapter(adapterGame);
    }
    public void f(){

    }
}