package pe.uni.mhuamanir.pc5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ImageView logoSplash;
    ImageView tittleSplash;
    Animation animLogo,animTittle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logoSplash=findViewById(R.id.logo);
        tittleSplash=findViewById(R.id.tittle);

        animLogo= AnimationUtils.loadAnimation(this,R.anim.logo_anim);
        animTittle=AnimationUtils.loadAnimation(this,R.anim.tittle_anim);

        logoSplash.setAnimation(animLogo);
        tittleSplash.setAnimation(animTittle);

        new CountDownTimer(2500,1000){

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent =new Intent(MainActivity.this,Login.class);
                startActivity(intent);
                finish();
            }
        }.start();
    }
}